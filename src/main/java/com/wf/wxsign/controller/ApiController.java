package com.wf.wxsign.controller;

import com.wf.wxsign.common.JsonResult;
import com.wf.wxsign.common.utils.HttpUtil;
import com.wf.wxsign.common.utils.JSONUtil;
import com.wf.wxsign.common.utils.UUIDUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by wangfan on 2018-12-13 下午 4:47.
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api")
@RestController
public class ApiController {

    // 获取微信JS-SDK签名
    @RequestMapping("/getJSSDKSignature")
    public JsonResult getJSSDKSignature(String appId, String secret, String url) {
        String tokenJson = HttpUtil.get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + secret, null);
        String access_token = JSONUtil.getString(tokenJson, "access_token");  // access_token
        String ticketJson = HttpUtil.get("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + access_token + "&type=jsapi", null);
        String ticket = JSONUtil.getString(ticketJson, "ticket");  // ticket
        String noncestr = UUIDUtil.randomUUID8();  // 随机字符串
        long timestamp = new Date().getTime();  // 时间戳
        String str = "jsapi_ticket=" + ticket;
        str += "&noncestr=" + noncestr;
        str += "&timestamp=" + timestamp;
        str += "&url=" + url;
        String signature = DigestUtils.sha1Hex(str);
        return JsonResult.ok().put("timestamp", timestamp).put("nonceStr", noncestr).put("signature", signature);
    }

    /**
     * access_token请求接口：
     * https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=xxx&secret=xxx
     * 返回示例：
     * {"access_token":"xxxxxx","expires_in":7200}
     *
     * ticket请求接口：
     * https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=xxx&type=jsapi
     * 返回示例：
     * {"errcode":0,"errmsg":"ok","ticket":"xxxxx","expires_in":7200}
     */
}
